package com.haydaa.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.JoinColumnOrFormula;

@Entity
@Table(name="SUBJECT")
public class Subject implements Serializable  {

	public Date getUpdateCommentDate() {
		return updateCommentDate;
	}

	public void setUpdateCommentDate(Date updateCommentDate) {
		this.updateCommentDate = updateCommentDate;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	private static final long serialVersionUID = -5527566248002296042L;
	
	@Id
	@Column(name="SUBJECT_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int subject_id;
	
	@Column(name="text")
	private String subject_text;

	@ManyToOne
	private User user;
	
	@OneToMany(mappedBy="subject",cascade=CascadeType.ALL)
	 private List<Comment> comments;
	 
	 @Column(name="createdDate")
	 private Date createdDate;
	 
	 @Column(name="updateCommentDate")
	 private Date updateCommentDate;
	 
	 @Column(name="isRead")
	 private Integer isRead;
	 
	 
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name="commentCount")
	private String comment_count;
	
	public int getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}

	public String getSubject_text() {
		return subject_text;
	}

	public void setSubject_text(String subject_text) {
		this.subject_text = subject_text;
	}


	public String getComment_count() {
		return comment_count;
	}

	public void setComment_count(String comment_count) {
		this.comment_count = comment_count;
	}






	
	
}
