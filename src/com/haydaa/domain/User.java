package com.haydaa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="USER")
public class User implements Serializable {

	 public Integer getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}
	public Integer getSubjectCount() {
		return subjectCount;
	}
	public void setSubjectCount(Integer subjectCount) {
		this.subjectCount = subjectCount;
	}
	public Integer getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
	private static final long serialVersionUID = -5527566248002296042L;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
    @Column(name="email", unique=true, nullable=false)
	private String email;
    @Column(name="userName", unique=true, nullable=false)
	private String user_name;
    
	@Column(name="code",nullable=false)
	private String code;
	
	@Column(name="gender",nullable=false)
	private String gender;
	
	@Column(name="university",nullable=false)
	private String university;
	
	@Column(name="department",nullable=false)
	private String department;
	
	@Column(name="rating")
	private String rating;
	
	@Column(name="regId")
	private String regId;
	
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	@Column(name="emailCode")
	private String emailCode;
	
	@Column(name="isActive")
	private Integer isActive;
	
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getEmailCode() {
		return emailCode;
	}
	public void setEmailCode(String emailCode) {
		this.emailCode = emailCode;
	}
	@Column(name="likeCount")
	private Integer likeCount;
	
	@Column(name="subjectCount")
	private Integer subjectCount;
	
	@Column(name="commentCount")
	private Integer commentCount;
	
	
	@Column(name="createdDate",nullable=false)
	private String created_date;
	
	@Column(name="graduationDate",nullable=false)
	private String graduation_date;
	
	@OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	 private List<Subject> subjects;
	 
	 @OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	 private List<Comment> comments;
	 
	 @OneToMany(mappedBy="likeUser",cascade=CascadeType.ALL)
	 private List<Like> likes;

	
	public List<Like> getLikes() {
		return likes;
	}
	public void setLikes(List<Like> likes) {
		this.likes = likes;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public List<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
	public Integer getUser_id() {
		return id;
	}
	public void setUser_id(Integer user_id) {
		this.id = user_id;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCode() {
		return code;
	}
	public void setPassword(String code) {
		this.code = code;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getDepartmant() {
		return department;
	}
	public void setDepartmant(String departmant) {
		this.department = departmant;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getGraduation_date() {
		return graduation_date;
	}
	public void setGraduation_date(String graduation_date) {
		this.graduation_date = graduation_date;
	}
	
	
	
}
