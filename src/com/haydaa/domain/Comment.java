package com.haydaa.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="COMMENT")
public class Comment implements Serializable  {

	private static final long serialVersionUID = -5527566248002296042L;
	
public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

@Id
@Column(name="COMMENT_ID")
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

@Column(name="text")
private String text;											

@Column(name="created_date")
private java.util.Date createdTime;

@Column(name="likeCount")
private Integer likeCount;

@ManyToOne
private User user;

@OneToMany(mappedBy="likeComment",cascade=CascadeType.ALL)
private List<Like> likes;

public List<Like> getLikes() {
	return likes;
}

public void setLikes(List<Like> likes) {
	this.likes = likes;
}

public User getUser() {
	return user;
}

public void setUser(User user) {
	this.user = user;
}

public Subject getSubject() {
	return subject;
}

public void setSubject(Subject subject) {
	this.subject = subject;
}

@ManyToOne
private Subject subject;



public Integer getLikeCount() {
	return likeCount;
}

public void setLikeCount(Integer likeCount) {
	this.likeCount = likeCount;
}

public java.util.Date getCreatedTime() {
	return createdTime;
}

public void setCreatedTime(java.util.Date createdTime) {
	this.createdTime = createdTime;
}


	
	
}
