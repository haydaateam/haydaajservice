package com.haydaa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LIKETABLE")
public class Like implements Serializable  {
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Long likeCount) {
		this.likeCount = likeCount;
	}

	public User getLikeUser() {
		return likeUser;
	}

	public void setLikeUser(User likeUser) {
		this.likeUser = likeUser;
	}

	public Comment getLikeComment() {
		return likeComment;
	}

	public void setLikeComment(Comment likeComment) {
		this.likeComment = likeComment;
	}

	private static final long serialVersionUID = -5527566248002296042L;
	@Id
	@Column(name="LIKE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="LIKE_COUNT")
	private Long likeCount;
	
	@ManyToOne
	private User likeUser;
	
	@ManyToOne
	private Comment likeComment;
	
}
