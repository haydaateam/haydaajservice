package com.haydaa.service;

import java.util.List;

import com.haydaa.domain.Department;
import com.haydaa.domain.University;

public interface UniversityDepartmentService {

	public List<University> getAllUniversities();
	public List<Department> getAllDepartments();
}
