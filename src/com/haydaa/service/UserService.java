package com.haydaa.service;

import com.haydaa.domain.User;

public interface UserService {

	public User getUser(String userEmail, String userPassword);

	public String newUser(String userEmail,String userName,String userUniversity, String userDepartment, String userGender,String userGradDate,String userCode,String registrationId);

	public User getUserById(int user_id);
	
	public String setIsActivated(int user_id,String emailCode);
	
}
