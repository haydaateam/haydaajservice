package com.haydaa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haydaa.dao.impl.SubjectDaoImpl;
import com.haydaa.domain.Subject;
import com.haydaa.domain.User;
import com.haydaa.service.SubjectService;

@Service
public class SubjectServiceImpl implements SubjectService {

	@Autowired
	private SubjectDaoImpl subjectDao;
	
	@Override
	@Transactional
	public Subject getSubjectById(int subject_id) {
		// TODO Auto-generated method stub
		return subjectDao.getSubjectById(subject_id);
	}

	@Override
	@Transactional
	public List<Subject> getSubjectsListByUserId(int user_id) {
		// TODO Auto-generated method stub
		return subjectDao.getSubjectsListByUserId(user_id);
	}

	@Override
	@Transactional
	public List<Subject> getPopularSubjects(int userId,String regId) {
		// TODO Auto-generated method stub
		return subjectDao.getPopularSubjects(userId,regId);
	}

	@Override
	@Transactional
	public List<Subject> getSubjectsByUniversity(String university) {
		// TODO Auto-generated method stub
		return subjectDao.getSubjectsByUniversity(university);
	}

	@Override
	@Transactional
	public String newSubject(int user_id, Subject subject) {
		// TODO Auto-generated method stub
		return subjectDao.newSubject(user_id, subject);
	}

	@Override
	@Transactional
	public String updateSubjectCommentCount(int subject_id, String count) {
		// TODO Auto-generated method stub
		return subjectDao.updateSubjectCommentCount(subject_id, count);
	}

	@Override
	@Transactional
	public List<Subject> getSubjectsByUniversityAndDepartmant(String university, String departmant) {
		// TODO Auto-generated method stub
		return subjectDao.getSubjectsByUniversityAndDepartmant(university, departmant);
	}

	@Override
	@Transactional
	public List<Subject> getSubjectListOrderByDate(int userId) {
		// TODO Auto-generated method stub
		return subjectDao.getSubjectListOrderByDate(userId);
	}
	
	@Override
	@Transactional
	public  List<Subject> getUserSubjectNotificationById(int user_id) {
		// TODO Auto-generated method stub
		return subjectDao.getUserSubjectNotificationById(user_id);
	}

}
