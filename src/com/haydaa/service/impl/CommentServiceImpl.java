package com.haydaa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haydaa.dao.CommentDao;
import com.haydaa.domain.Comment;
import com.haydaa.domain.Subject;
import com.haydaa.service.CommentService;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {
@Autowired
private CommentDao commentDao;
	
	@Override
	public List<Comment> getPopularCommentListBySubjectId(int subject_id) {
		// TODO Auto-generated method stub
		return commentDao.getPopularCommentListBySubjectId(subject_id);
	}

	@Override
	public Comment getCommentById(int comment_id) {
		// TODO Auto-generated method stub
		return commentDao.getCommentById(comment_id);
	}

	@Override
	public String newComment(int user_id, int subject_id, Comment comment) {
		// TODO Auto-generated method stub
		return commentDao.newComment(user_id, subject_id, comment);
	}

	@Override
	public List<Comment> getCommentListBySubjectId(int subject_id) {
		// TODO Auto-generated method stub
		return commentDao.getCommentListBySubjectId(subject_id);
	}

	@Override
	public String setLike(Integer comment_id,Integer user_id) {
		// TODO Auto-generated method stub
		return commentDao.setLike(comment_id,user_id);
	}

	@Override
	public  List<Subject> getCommentSubjectListByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return commentDao.getCommentSubjectListByUserId(userId);
	}

}
