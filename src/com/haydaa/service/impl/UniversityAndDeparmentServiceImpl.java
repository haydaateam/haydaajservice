package com.haydaa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haydaa.dao.UniversityDepartmentDao;
import com.haydaa.domain.Department;
import com.haydaa.domain.University;
import com.haydaa.service.UniversityDepartmentService;

@Service
@Transactional
public class UniversityAndDeparmentServiceImpl implements UniversityDepartmentService {

	@Autowired
	UniversityDepartmentDao universityDeparmentDao;
	
	@Override
	public List<University> getAllUniversities() {
		// TODO Auto-generated method stub
		return universityDeparmentDao.getAllUniversities();
	}

	@Override
	public List<Department> getAllDepartments() {
		// TODO Auto-generated method stub
		return universityDeparmentDao.getAllDepartments();
	}

}
