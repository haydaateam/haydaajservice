package com.haydaa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haydaa.dao.impl.UserDaoImpl;
import com.haydaa.domain.User;
import com.haydaa.service.UserService;

@Service
public class UserSeviceImpl implements UserService {

	@Autowired
	UserDaoImpl userDao;
	
	@Override
	@Transactional
	public User getUser(String userEmail, String userPassword) {
		// TODO Auto-generated method stub
		if(userDao!=null){
			return userDao.getUser(userEmail, userPassword);
		}
		return null;
	}

	@Override
	@Transactional
	public String newUser(String userEmail,String userName,String userUniversity, String userDepartment, String userGender,String userGradDate,String userCode,String registrationId) {
		// TODO Auto-generated method stub
		
		if(userDao!=null){
			return userDao.newUser(userEmail, userName, userUniversity,userDepartment,userGender,userGradDate,userCode,registrationId);
		}
		return null;
		
	}

	@Override
	@Transactional
	public User getUserById(int user_id) {
		// TODO Auto-generated method stub
		if(userDao!=null){
			return userDao.getUserById(user_id);
		}
		return null;
	}
	
	@Override
	@Transactional
	public String setIsActivated(int user_id, String emailCode) {
		// TODO Auto-generated method stub
		if(userDao!=null){
			return userDao.setIsActivated(user_id, emailCode);
		}
		return null;
	}

}
