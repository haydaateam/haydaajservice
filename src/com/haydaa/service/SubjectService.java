package com.haydaa.service;

import java.util.List;

import com.haydaa.domain.Subject;
import com.haydaa.domain.User;

public interface SubjectService {
	public Subject getSubjectById(int subject_id);
	public List<Subject> getSubjectsListByUserId(int user_id);
	public List<Subject> getPopularSubjects(int userId,String regId);
	public List<Subject> getSubjectsByUniversity(String university);
	public String newSubject(int user_id, Subject subject);
	public List<Subject> getSubjectsByUniversityAndDepartmant(String university,String departmant);
	public String updateSubjectCommentCount(int subject_id,String count);
	public List<Subject> getSubjectListOrderByDate(int userId);
	public  List<Subject> getUserSubjectNotificationById(int user_id);
}
