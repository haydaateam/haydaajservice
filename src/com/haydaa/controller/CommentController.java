package com.haydaa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haydaa.domain.Comment;
import com.haydaa.domain.Subject;
import com.haydaa.model.BaseResponse;
import com.haydaa.service.CommentService;

@Controller 
public class CommentController {

	@Autowired 
	private CommentService commentService;
	
	@RequestMapping(value="/popularCommentList{subjectId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getPopularSubjectListById(@RequestParam("subjectId")int subjectId){
		BaseResponse baseResponse=new BaseResponse();
		if(commentService.getPopularCommentListBySubjectId(subjectId)!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Comment");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(commentService.getPopularCommentListBySubjectId(subjectId));
	return baseResponse;
	}
	
	@RequestMapping(value="/commentSubjectListByUser{userId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getCommentSubjectListById(@RequestParam("userId")Integer userId){
		BaseResponse baseResponse=new BaseResponse();
		Object object=commentService.getCommentSubjectListByUserId(userId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Comment");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	
	@RequestMapping(value="/commentById",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getCommentById(@RequestParam("comment-id")Integer commentId){
		BaseResponse baseResponse=new BaseResponse();
		if(commentService.getCommentById(commentId)!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Comment");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(commentService.getCommentById(commentId));
	return baseResponse;
	}
	
	
	
	
	@RequestMapping(value="/commentListBySubjectId{subjectId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectListById(@RequestParam("subjectId")Integer subjectId){
		BaseResponse baseResponse=new BaseResponse();
		Object object=commentService.getCommentListBySubjectId(subjectId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Comment");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
		
	return baseResponse;
	}
	
	@RequestMapping(value = "/saveComment",method=RequestMethod.POST)
	@ResponseBody
	public BaseResponse saveSubject(@RequestHeader("text")String text,@RequestHeader("user-id")Integer userId
			,@RequestHeader("subject-id")Integer subjectId){
		
		BaseResponse baseResponse=new BaseResponse();
		
		Comment comment=new Comment();
		comment.setText(text);
		comment.setLikeCount(0);
	
		if(!commentService.newComment(userId, subjectId, comment).equals("200")){
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
		  baseResponse.setResponseObject(null);
		  baseResponse.setResponseType("WARN");
		}
		else{
			baseResponse.setResponseCode("200");
			 baseResponse.setMessage("success");
			  baseResponse.setResponseObject(null);
			  baseResponse.setResponseType("INFO");
		}
		
		return baseResponse;
	}
	
	@RequestMapping(value = "/setLike",method=RequestMethod.POST)
	@ResponseBody
	public BaseResponse saveSubject(@RequestHeader("comment-id")Integer commentId,@RequestHeader("user-id")Integer userId){
		
		BaseResponse baseResponse=new BaseResponse();
	
	
		if(!commentService.setLike(commentId,userId).equals("200")){
		  baseResponse.setResponseCode("201");
		  baseResponse.setMessage("fail");
		  baseResponse.setResponseObject(null);
		  baseResponse.setResponseType("WARN");
		}
		else{
			 baseResponse.setResponseCode("200");
			 baseResponse.setMessage("success");
			  baseResponse.setResponseObject(null);
			  baseResponse.setResponseType("INFO");
		}
		return baseResponse;
	}
	
}
