package com.haydaa.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.haydaa.domain.User;
import com.haydaa.model.BaseResponse;
import com.haydaa.service.impl.UserSeviceImpl;
import com.haydaa.util.Utils;


@Controller
public class UserController {

	@Autowired
	UserSeviceImpl userService;
	
	
	@RequestMapping(value = "/userById{userId}",method=RequestMethod.GET)
	@ResponseBody
	public BaseResponse getUserById(@RequestParam("userId")int userId){
		BaseResponse baseResponse=new BaseResponse();
		
		if(userService.getUserById(userId)==null){
			baseResponse.setResponseCode("201");
			baseResponse.setMessage(null);
			baseResponse.setResponseType("Warning");
		}else{
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("User");
		}
		baseResponse.setResponseObject(userService.getUserById(userId));
		
		return baseResponse;
	}
	
	@RequestMapping(value = "/userByName{username}{code}",method=RequestMethod.GET)
	@ResponseBody
	public BaseResponse getUserByNameAndCode(@RequestParam("name")String name,@RequestParam("code")String code){
		
	BaseResponse baseResponse=new BaseResponse();
		Object object=userService.getUser(name, code);
		if(object==null){
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType("Warning");
		}else{
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("User");
		}
		baseResponse.setResponseObject(object);
		
		return baseResponse;
	
		
	}
	
	@RequestMapping(value = "/saveUser",method=RequestMethod.POST,produces="application/json")
	@ResponseBody
	public BaseResponse saveUser(@RequestHeader("email")String userEmail,@RequestHeader("userName")String userName,
			@RequestHeader("userUniversity")String userUniversity,@RequestHeader("userDepartment")String userDepartment,
			@RequestHeader("userGender")String userGender,@RequestHeader("userGradDate")String userGradDate,@RequestHeader("userCode")String userCode,@RequestHeader("regId")String regId){
		
		BaseResponse baseResponse=new BaseResponse();
		
		if(!userService.newUser(userEmail, userName, userUniversity, userDepartment, userGender, userGradDate, userCode,regId).equals("200")){
		  baseResponse.setResponseCode("201");
		  baseResponse.setResponseObject(null);
		  baseResponse.setMessage("fail");
		  baseResponse.setResponseType("WARN");
		}
		else{
			 baseResponse.setResponseCode("200");
			  baseResponse.setResponseObject(null);
			  baseResponse.setMessage("Haydaa, Art�k �yemizsin.");
			  baseResponse.setResponseType("INFO");
		}
		return baseResponse;
	}
	
	
	@RequestMapping(value = "/activate{id}{code}",method=RequestMethod.GET)
	public ModelAndView setActivate(@RequestParam("id")Integer id,@RequestParam("code")String code){
		
		ModelAndView model = new ModelAndView("ActivationPage");
		Object object=userService.setIsActivated(id,code);
		if(object.equals("201")){
			model.addObject("msg","Aktivasyon isleminiz basarisizdir.");
		}else{
			model.addObject("msg","Aktivasyon isleminiz basarilidir.");
		}
		
		return model;
	
		
	}
	
	
	

}
