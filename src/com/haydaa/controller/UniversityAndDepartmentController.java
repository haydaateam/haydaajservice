package com.haydaa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haydaa.model.BaseResponse;
import com.haydaa.service.UniversityDepartmentService;

@Controller
public class UniversityAndDepartmentController {

	@Autowired
	UniversityDepartmentService universityDepartmentService;
	
	@RequestMapping(value="/universityList",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getUniversityList(){
		BaseResponse baseResponse=new BaseResponse();
		Object object=universityDepartmentService.getAllUniversities();
		if(object != null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	
	@RequestMapping(value="/departmentList",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getDepartmentList(){
		BaseResponse baseResponse=new BaseResponse();
		if(universityDepartmentService.getAllDepartments()!= null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(universityDepartmentService.getAllDepartments());
	return baseResponse;
	}
}
