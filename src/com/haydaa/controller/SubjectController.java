package com.haydaa.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haydaa.domain.Subject;
import com.haydaa.model.BaseResponse;
import com.haydaa.service.SubjectService;
import com.haydaa.service.UserService;
import com.haydaa.service.impl.SubjectServiceImpl;
import com.haydaa.service.impl.UserSeviceImpl;

@Controller
public class SubjectController {

	@Autowired
	SubjectService subjectService;
	
	@Autowired
    UserService userService;
	
	@RequestMapping(value="/subjectById{subjectId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectById(@RequestParam("subjectId")int subjectId){
		BaseResponse baseResponse=new BaseResponse();
Object object=subjectService.getSubjectById(subjectId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value="/subjectListByUserId{userId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectListById(@RequestParam("userId")int userId){
		BaseResponse baseResponse=new BaseResponse();
		Object object=subjectService.getSubjectsListByUserId(userId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value="/subjectPopularList{userId}{regId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getPopularSubjectListById(@RequestParam("userId")int userId,@RequestParam("regId")String regId){
		BaseResponse baseResponse=new BaseResponse();
		Object object=subjectService.getPopularSubjects(userId,regId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value="/subjectListOrderBy{userId}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectListOrderBy(@RequestParam("userId")int userId){
		BaseResponse baseResponse=new BaseResponse();
		Object object=subjectService.getSubjectListOrderByDate(userId);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value="/subjectListByUniversity{universityName}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectListById(@RequestParam("universityName")String university){
		BaseResponse baseResponse=new BaseResponse();
		Object object=subjectService.getSubjectsByUniversity(university);
		if(object!=null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value="/ByUniversityAndDepartmentSubjectList{universityName}{dapartmentName}",method=RequestMethod.GET)
	public @ResponseBody BaseResponse getSubjectListById(@RequestParam("universityName")String university,@RequestParam("dapartmentName")String departmentName){
		BaseResponse baseResponse=new BaseResponse();
		Object object=subjectService.getSubjectsByUniversityAndDepartmant(university,departmentName);
		if(object != null){
			baseResponse.setResponseCode("200");
			baseResponse.setMessage("success");
			baseResponse.setResponseType("Subject");
		}else{
			baseResponse.setResponseCode("201");
			baseResponse.setMessage("fail");
			baseResponse.setResponseType(null);
		}
		baseResponse.setResponseObject(object);
	return baseResponse;
	}
	
	@RequestMapping(value = "/saveSubject",method=RequestMethod.POST)
	@ResponseBody
	public BaseResponse saveSubject(@RequestHeader("text")String text,@RequestHeader("user-id")Integer userId){
		
		BaseResponse baseResponse=new BaseResponse();
		
		Date date = new Date();
		Subject subject=new Subject();
		subject.setSubject_text(text);
		subject.setComment_count("0");
		subject.setCreatedDate(date);
		
		if(!subjectService.newSubject(userId,subject).equals("200")){
		  baseResponse.setResponseCode("201");
		  baseResponse.setResponseObject(null);
		  baseResponse.setResponseType("WARN");
		}
		else{
			 baseResponse.setResponseCode("200");
			 baseResponse.setMessage("Yeni konun paylaşıldı.");
			  baseResponse.setResponseObject(null);
			  baseResponse.setResponseType("INFO");
		}
		return baseResponse;
	}
	
	@RequestMapping(value = "/updateCommentCount",method=RequestMethod.POST)
	@ResponseBody
	public BaseResponse updateComment(@RequestHeader("subject-id")Integer subject_id,@RequestHeader("count")String count){
		
		BaseResponse baseResponse=new BaseResponse();
		
	
		if(!subjectService.updateSubjectCommentCount(subject_id, count).equals("200")){
		  baseResponse.setResponseCode("201");
		  baseResponse.setResponseObject(null);
		  baseResponse.setMessage("fail");
		  baseResponse.setResponseType("WARN");
		}
		else{
			 baseResponse.setResponseCode("200");
			  baseResponse.setResponseObject(null);
			  baseResponse.setMessage("success");
			  baseResponse.setResponseType("INFO");
		}
		return baseResponse;
	}
	
	@RequestMapping(value = "/subjectNotificationsByUser{userId}",method=RequestMethod.GET)
	@ResponseBody
	public BaseResponse updateComment(@RequestParam("userId")Integer userId){
		
		BaseResponse baseResponse=new BaseResponse();
		
		Object object=subjectService.getUserSubjectNotificationById(userId);
	
		if(object == null){
		  baseResponse.setResponseCode("201");
		  baseResponse.setResponseObject(null);
		  baseResponse.setMessage("fail");
		  baseResponse.setResponseType("WARN");
		}
		else{
			 baseResponse.setResponseCode("200");
			  baseResponse.setResponseObject(object);
			  baseResponse.setMessage("success");
			  baseResponse.setResponseType("INFO");
		}
		return baseResponse;
	}
}
