package com.haydaa.util;

public class Utils {

	   public static String convertEncodeCharacters(String text) {
	        text = text.replace("�","\\u0131");
	        text=text.replace("�","\\u015F");
	        text=text.replace("�","\\u011F");
	        text=text.replace("�","\\u0130");
	        text=text.replace(" ","%20");
	        text=text.replace("�","%FC");
	        text=text.replace("�","%DC");
	        return text;
	    }

	    public static String convertDecodeCharacters(String text) {

	        text = text.replace("\\u0131","�");
	        text=text.replace("\\u015F","�");
	        text=text.replace("\\u011F","�");
	        text=text.replace("\\u0130","�");
	        text=text.replace("%FC","�");
	        text=text.replace("%DC","�");
	        text=text.replace("%20"," ");
	        return text;
	    }
}
