package com.haydaa.dao;

import java.util.List;

import com.haydaa.domain.Comment;
import com.haydaa.domain.Subject;

public interface CommentDao {

	public List<Comment> getPopularCommentListBySubjectId(int subject_id);
	public  List<Subject> getCommentSubjectListByUserId(Integer userId);
	public Comment getCommentById(int comment_id);
	public String newComment(int user_id,int subject_id,Comment comment);
	public List<Comment> getCommentListBySubjectId(int subject_id);
	public String setLike(Integer comment_id,Integer user_id);
	
	
}
