package com.haydaa.dao;

import java.util.List;

import com.haydaa.domain.Department;
import com.haydaa.domain.University;

public interface UniversityDepartmentDao {

	public List<University> getAllUniversities();
	public List<Department> getAllDepartments();
}
