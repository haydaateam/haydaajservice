package com.haydaa.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haydaa.dao.UniversityDepartmentDao;
import com.haydaa.domain.Department;
import com.haydaa.domain.University;

@Repository
public class UniversityAndDepartmentDaoImpl implements UniversityDepartmentDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public List<University> getAllUniversities() {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		Query query=session.createQuery("from University order by name");
		
		if(query.list().isEmpty()){
			return null;
		}
		
		return (List<University>)query.list();
	}

	@Override
	public List<Department> getAllDepartments() {
		Session session=getCurrentSession();
		Query query=session.createQuery("from Department order by name");
		
		if(query.list().isEmpty()){
			return null;
		}
		
		
		return (List<Department>)query.list();
	}

}
