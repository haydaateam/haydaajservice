package com.haydaa.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haydaa.dao.CommentDao;
import com.haydaa.domain.Comment;
import com.haydaa.domain.Like;
import com.haydaa.domain.Subject;
import com.haydaa.domain.User;
import com.haydaa.util.Utils;

@Repository
public class CommentDaoImpl implements CommentDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public List<Comment> getPopularCommentListBySubjectId(int subject_id) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		Query query=session.createQuery("select c from Comment c where c.subject.subject_id = "+subject_id+" order by likeCount desc");
		List<Comment> queryList=new ArrayList();
		queryList= (List<Comment>)query.list();
		return queryList;
	}

	@Override
	public Comment getCommentById(int comment_id) {
		Session session=getCurrentSession();
		Comment comment=(Comment)session.get(Comment.class, comment_id);
		if (comment!=null){
			return comment;
		}
		return null;
	}

	@Override
	public String newComment(int user_id, int subject_id, Comment comment) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		comment.setCreatedTime(date);
		User user=(User)session.get(User.class,user_id);
		Subject subject=(Subject)session.get(Subject.class, subject_id);
		if(user!=null && subject!=null && Utils.convertDecodeCharacters(comment.getText()).length()<201){
			comment.setUser(user);
			int count=Integer.parseInt(subject.getComment_count())+1;
			subject.setComment_count(String.valueOf(count));
			comment.setSubject(subject);
		    subject.setUpdateCommentDate(date);
		    subject.setIsRead(0);
			session.persist(comment);
			NotificationService notification=new NotificationService();
			if(subject.getUser().getRegId()!=null){
					if(!subject.getUser().getRegId().equals("") && !user.getRegId().equals(subject.getUser().getRegId())){
						notification.pushNotificationToGCM(String.valueOf(subject.getUser().getRegId()),
								user.getUser_name()+" a�t���n�z konuya yorum yapt�");
					
				}
			}
			
			
			
			List<String> regIds=new ArrayList();
			for(Comment comments:subject.getComments()){
				if(comments.getUser().getRegId()!=null){
					if( !user.getRegId().equals(comments.getUser().getRegId()) &&  !comments.getUser().getRegId().equals(""))
					regIds.add(comments.getUser().getRegId());
				}
			}
			if(!regIds.isEmpty())
			 notification.pushMultiNotificationToGCM(regIds, "Takip etti�in konuya yorumlar yap�l�yor");
			
			
			return "200";	
		}
		return "201";
		
	}

	@Override
	public List<Comment> getCommentListBySubjectId(int subject_id) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
	
		Query query=session.createQuery("select c from Comment c where c.subject.subject_id="+subject_id+"");
		List<Comment> commentList=new ArrayList<Comment>();
		commentList=(List<Comment>)query.list();
		
		for(Comment comment:commentList){
			User user=new User();
			Subject newSubject=new Subject();
			newSubject.setSubject_id(comment.getSubject().getSubject_id());
			user.setUser_id(comment.getUser().getUser_id());
			user.setUser_name(comment.getUser().getUser_name());
			user.setUniversity(comment.getUser().getUniversity());
			user.setDepartmant(comment.getUser().getDepartmant());
			user.setGender(comment.getUser().getGender());
			comment.setUser(user);
			comment.setSubject(newSubject);
			comment.setLikes(null);
		}
		return commentList;
	}

	@Override
	public String setLike(Integer comment_id,Integer user_id) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		
		Comment comment=(Comment)session.get(Comment.class, comment_id);
		User user=(User)session.get(User.class, user_id);
		Query query=session.createQuery("select l from Like l where l.likeComment.id="+comment_id+" AND l.likeUser.id="+user_id);
		if( query.list().isEmpty()){
			if(comment!=null && user!=null){
				Like like=new Like();
				like.setLikeCount(Long.parseLong(String.valueOf(comment.getLikeCount()+1)));
				like.setLikeComment(comment);
				like.setLikeUser(user);
				session.persist(like);
				comment.setLikeCount(Integer.parseInt(String.valueOf(like.getLikeCount())));
				session.update(comment);
				return "200";
			}
		return "201";
		}
		
		return "201";
	}

	@Override
	public List<Subject> getCommentSubjectListByUserId(Integer userId) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		User user=(User)session.get(User.class, userId);
		
		
		Set<Subject> subjectSet=new HashSet<Subject>();
		if(user!=null){
			for(Comment comment : user.getComments()){
				
				User subjectUser=comment.getSubject().getUser();
				User newUser=subjectUser;
				newUser.setComments(null);
				newUser.setSubjects(null);
				newUser.setLikes(null);
				
				comment.getSubject().setComments(null);
				comment.getSubject().setUser(newUser);
				subjectSet.add(comment.getSubject());
			}
		}
		
		List<Subject> queryList=new ArrayList(subjectSet);
		
		if(queryList.isEmpty()){
			return null;
		}
		
		return queryList;
	}

}
