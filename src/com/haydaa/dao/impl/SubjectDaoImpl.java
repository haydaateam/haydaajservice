package com.haydaa.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haydaa.dao.SubjectDao;
import com.haydaa.domain.Comment;
import com.haydaa.domain.Subject;
import com.haydaa.domain.User;
import com.haydaa.util.Utils;


@Repository
public class SubjectDaoImpl implements SubjectDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public Subject getSubjectById(int subject_id) {
		// TODO Auto-generated method stub
		Session session = getCurrentSession();
		Subject newSubject=new Subject();
		Subject subject = (Subject) session.get(Subject.class, subject_id);
		if(subject!=null){
			newSubject.setComment_count(subject.getComment_count());
			newSubject.setSubject_id(subject.getSubject_id());
			newSubject.setSubject_text(subject.getSubject_text());
			User u=new User();
			u.setUser_name(subject.getUser().getUser_name());
			u.setGender(subject.getUser().getGender());
			u.setUniversity(subject.getUser().getUniversity());
			u.setDepartmant(subject.getUser().getDepartmant());
			newSubject.setUser(u);
			subject.setIsRead(1);
		}else{
			newSubject=null;
		}
		
		return newSubject;
	}

	@Override
	public List<Subject> getSubjectsListByUserId(int user_id) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		Query query=session.createQuery("SELECT DISTINCT u FROM User as u LEFT JOIN FETCH u.subjects AS subjects WHERE u.id="+user_id+" ORDER BY subjects.updateCommentDate DESC");
		User user = (User) query.uniqueResult();
		   
		if(user==null){
			return null;
		}
		
		  for(int i=0;i<user.getSubjects().size();i++){
			  User newUser=new User();
			  newUser.setUser_id(user.getUser_id());
			  newUser.setUser_name(user.getUser_name());
			  newUser.setUniversity(user.getUniversity());
			  newUser.setDepartmant(user.getDepartmant());
			  newUser.setGender(user.getGender());
			  user.getSubjects().get(i).setUser(newUser);
			  user.getSubjects().get(i).setComments(null);
		  }
		  
		  // Retrieve all
		  return  new ArrayList<Subject>(user.getSubjects());
	}

	@Override
	public List<Subject> getPopularSubjects(int userId,String regId) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		User user=(User)session.get(User.class, userId);
		
		if(!regId.equals("")){
			user.setRegId(regId);
		}
		Query query=session.createQuery("select s from Subject s where s.user.university like '"+user.getUniversity()+"%'order by s.comment_count desc");
		List<Subject> queryList=new ArrayList();
		queryList= (List<Subject>)query.list();
		for(Subject s:queryList){
			User u=new User();
			u.setUser_id(s.getUser().getUser_id());
			u.setUser_name(s.getUser().getUser_name());
			u.setUniversity(s.getUser().getUniversity());
			u.setDepartmant(s.getUser().getDepartmant());
			u.setGender(s.getUser().getGender());
			s.setUser(u);
			s.setComments(null);
		}
		return queryList;
	}

	@Override
	public List<Subject> getSubjectsByUniversity(String university) {
		Session session=getCurrentSession();
		university=Utils.convertEncodeCharacters(university);
		Query query=session.createQuery("from User as u LEFT JOIN FETCH u.subjects where u.university like '"+university+"%'");

		List<User> userList = (List<User>) query.list();
		   
		  if(userList.isEmpty()){
			  return null;
		  }
		
		  for(User user : userList){
			  for(int i=0;i<user.getSubjects().size();i++){
				  User newUser=new User();
				  newUser.setUser_id(user.getUser_id());
				  newUser.setUser_name(user.getUser_name());
				  newUser.setUniversity(user.getUniversity());
				  newUser.setDepartmant(user.getDepartmant());
				  newUser.setGender(user.getGender());
				  user.getSubjects().get(i).setUser(newUser);
				  user.getSubjects().get(i).setComments(null);
			  }
		  }
		  List<Subject> subjectList=new ArrayList<Subject>();
		 for(User user : userList){
			 for(Subject subject : user.getSubjects())
			 subjectList.add(subject);
		 }
		  
        
		  return subjectList;
	}

	@Override
	public String newSubject(int user_id, Subject subject) {
		// TODO Auto-generated method stu
		Session session = getCurrentSession();
	   
	
		User user=(User)session.get(User.class, user_id);
		if(user!=null && Utils.convertDecodeCharacters(subject.getSubject_text()).length()<201){
			
			   subject.setUser(user);
				session.persist(subject);
				
				
			return "200";
		}
		else{
			return "201";
		}
		
	
	}

	@Override
	public String updateSubjectCommentCount(int subject_id, String count) {
		// TODO Auto-generated method stub
		Session session = getCurrentSession();
		Subject subject = (Subject) session.get(Subject.class, subject_id);
		if(subject!=null){
			subject.setComment_count(count);
			session.update(subject);
			return "200";
		}else{
			return "201";
		}
		
	
	}

	@Override
	public List<Subject> getSubjectsByUniversityAndDepartmant(String university, String departmant) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		university=Utils.convertEncodeCharacters(university);
		departmant=Utils.convertEncodeCharacters(departmant);
		Query query=session.createQuery("from User as u LEFT JOIN FETCH u.subjects where u.university like '"+university+"%' AND u.department like '"+departmant+"%' order by s.createdDate desc");
		  User user = (User) query.uniqueResult();
		  
		   if(user==null){
			   return null;
		   }
		  
		  for(int i=0;i<user.getSubjects().size();i++){
			  User newUser=new User();
			  newUser.setUser_id(user.getUser_id());
			  newUser.setUser_name(user.getUser_name());
			  newUser.setUniversity(user.getUniversity());
			  newUser.setDepartmant(user.getDepartmant());
			  newUser.setGender(user.getGender());
			  user.getSubjects().get(i).setUser(newUser);
			  user.getSubjects().get(i).setComments(null);
		  }
		 
		  List<Subject> subjectList=new ArrayList<Subject>(user.getSubjects());
        
		  
		  // Retrieve all
		  return  subjectList;
	}

	@Override
	public List<Subject> getSubjectListOrderByDate(int userId) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Session session=getCurrentSession();
				User user=(User)session.get(User.class, userId);
				Query query=session.createQuery("select s from Subject s where s.user.university like '"+user.getUniversity()+"%' order by s.createdDate desc");
				List<Subject> queryList=new ArrayList();
				queryList= (List<Subject>)query.list();
				for(Subject s:queryList){
					User u=new User();
					u.setUser_id(s.getUser().getUser_id());
					u.setUser_name(s.getUser().getUser_name());
					u.setUniversity(s.getUser().getUniversity());
					u.setDepartmant(s.getUser().getDepartmant());
					u.setGender(s.getUser().getGender());
					s.setUser(u);
					s.setComments(null);
				}
				return queryList;
	}
	
	@Override
	public List<Subject> getUserSubjectNotificationById(int user_id) {
		// TODO Auto-generated method stub
		Session session = getCurrentSession();
		Subject newSubject=new Subject();
		User user = (User) session.get(User.class, user_id);
		
		List<Subject> subjectList=new ArrayList();
		//subjectList=null;
		
		if(user == null){
			return null;
		}
		
		if(user.getSubjects().isEmpty()){
			return null;
		}
		
		for(Subject subject : user.getSubjects()){
			Subject subjectOfUser=(Subject)session.get(Subject.class, subject.getSubject_id());
		    newSubject.setSubject_id(subjectOfUser.getSubject_id());
		    newSubject.setCreatedDate(subjectOfUser.getCreatedDate());
		    newSubject.setComment_count(subjectOfUser.getComment_count());
		    subjectList.add(newSubject);
		 }
		
		return subjectList;
	}

}
