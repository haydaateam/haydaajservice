package com.haydaa.dao.impl;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.omg.CORBA.ExceptionList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Repository;

import com.haydaa.dao.UserDao;
import com.haydaa.domain.Comment;
import com.haydaa.domain.Subject;
import com.haydaa.domain.User;

import com.haydaa.util.SecurityUtil;


@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public User getUser(String userName, String userCode) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		
		Query query=session.createQuery("from User u where u.user_name like '"+ userName + "'");
		if(query.list().size()==0){
			return null;
		}else{
			User user=(User)query.list().get(0);
			try {
				if(user.getIsActive() != 1){
					return null;
				}else{
					if(SecurityUtil.getInstance().encryptedAES(userCode)
							.equals(SecurityUtil.getInstance().decryptAES(user.getCode()))){
						User newUser=new User();
						newUser.setUser_id(user.getUser_id());
						newUser.setUser_name(user.getUser_name());
						newUser.setUniversity(user.getUniversity());
						newUser.setGender(user.getGender());
						newUser.setDepartmant(user.getDepartmant());
						newUser.setCommentCount(user.getComments().size());
						newUser.setLikeCount(user.getLikes().size());
						newUser.setSubjectCount(user.getSubjects().size());
						return newUser;
							}
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		return null;
		
	}
	
	@Autowired
	MailSender mailSender;

	@Override
	public String newUser(String userEmail,String userName,String userUniversity, String userDepartment, String userGender,String userGradDate,String userCode,String registrationId) {
		// TODO Auto-generated method stub
		

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		Session session = getCurrentSession();
		User user=new User();
		user.setEmail(userEmail);
	    user.setGender(userGender);
	    user.setUniversity(userUniversity);
	    user.setGraduation_date(userGradDate);
	    user.setDepartmant(userDepartment);
	    user.setUser_name(userName);
	    user.setRegId(registrationId);
	    user.setIsActive(0);
		String code=SecurityUtil.getInstance().randomString(32); //for email
	    user.setEmailCode(code);
	    try {
			user.setPassword(SecurityUtil.getInstance().encryptedAES(userCode));
			user.setCreated_date(dateFormat.format(date));
			user.setIsActive(1);
		    session.persist(user);
			/*SimpleMailMessage message = new SimpleMailMessage();

			message.setFrom("haydaainfo@gmail.com");
			message.setTo(userEmail);
			message.setSubject("Haydaa Do�rulama Linki");
			message.setText("Merhaba "+userName+", i�te do�rulama linkiniz; http://146.185.179.198:8080/HaydaaService/activate?id="+user.getUser_id()+"&code="+code);
			mailSender.send(message);
		    */
			
			return "200";
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
			}
			// TODO Auto-generated catch block
		

	
		return "201";
	}

	@Override
	public User getUserById(int user_id) {
		// TODO Auto-generated method stub
		Session session = getCurrentSession();
		User newUser=new User();
		User user = (User) session.get(User.class, user_id);
		
		
		
		if(user != null){
			int sumComment=0;
			int sumLike=0;
			for(Subject subject : user.getSubjects()){
				 sumComment+=subject.getComments().size();
				 for(Comment comment : subject.getComments()){
					 sumLike+=comment.getLikeCount();
				 }
			}
			
			
			newUser.setUser_id(user.getUser_id());
			newUser.setUser_name(user.getUser_name());
			newUser.setUniversity(user.getUniversity());
			newUser.setGender(user.getGender());
			newUser.setDepartmant(user.getDepartmant());
			newUser.setCommentCount(sumComment);
			newUser.setLikeCount(sumLike);
			newUser.setSubjectCount(user.getSubjects().size());
			
		}else {
			newUser=null;
		}
		
		return newUser ;
	}
	
	@Override
	public String setIsActivated(int user_id, String emailCode) {
		// TODO Auto-generated method stub
		Session session=getCurrentSession();
		User user=(User)session.get(User.class, user_id);
		
		if(user != null){
			if(user.getEmailCode().equals(emailCode)){
				user.setIsActive(1);
				session.update(user);
				return "200";
				}
		}
		

		return "201";
	}
	

}
